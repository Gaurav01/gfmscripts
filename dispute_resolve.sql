﻿
CREATE OR REPLACE FUNCTION dispute_accept(
    in_recharge_tran_his_id integer,
    in_recharge_tran_id bigint,
    in_tran_by integer)
  RETURNS integer AS
$BODY$
DECLARE
_record record;
temp_wallet_balance numeric;
BEGIN



FOR _record IN (SELECT * FROM user_commision WHERE recharge_tran_his_id_id=in_recharge_tran_his_id) LOOP 

	-- Take present wallet balance
	SELECT wallet_amount INTO temp_wallet_balance FROM wallet WHERE wallet_user_id_id=_record.user_id_id;
	
	IF _record.commision_status = 10 THEN
	     INSERT INTO user_commision( recharge_amount, commision_percentage, commision_tax, commision_amount_final, commision_status, commision_datetime, 
             recharge_api_id_id, recharge_tran_his_id_id, telecom_operator_id_id, user_id_id,user_commision_remarks)
             VALUES(_record.recharge_amount, (0-_record.commision_percentage),(0- _record.commision_tax), (0-_record.commision_amount_final), 2, now(), 
             _record.recharge_api_id_id, _record.recharge_tran_his_id_id, _record.telecom_operator_id_id, _record.user_id_id,'Dispute');
             INSERT INTO admin_commission_wallet( recharge_amount, commision_percentage, commision_tax, commision_amount_final, commision_status, commision_datetime, 
             recharge_api_id_id, recharge_tran_his_id_id, telecom_operator_id_id, user_id_id,user_commision_remarks)
             VALUES(_record.recharge_amount, (0-_record.commision_percentage),(0- _record.commision_tax), (0-_record.commision_amount_final), 2, now(), 
             _record.recharge_api_id_id, _record.recharge_tran_his_id_id, _record.telecom_operator_id_id, _record.user_id_id,'Dispute');
             
	    IF  _record.user_id_id= in_tran_by THEN
		INSERT INTO wallet_transaction(created_at,updated_at,reference_id,reference_type,transaction_type,transaction_amount,transaction_datetime, 
		created_by_id,transaction_by_id,wallet_transfer_from_user_id_id,wallet_transfer_to_user_id_id,updated_by_id,transaction_closing_amount,transaction_opening_amount)
		VALUES (now(),now(),in_recharge_tran_id ,50,'C',(_record.recharge_amount - _record.commision_amount_final),now(),1,1,_record.user_id_id,
		1,_record.user_id_id,temp_wallet_balance+_record.recharge_amount, temp_wallet_balance);

		UPDATE wallet  SET updated_at=now(), wallet_amount=wallet_amount+(_record.recharge_amount - _record.commision_amount_final), 
		wallet_update_datetime=now(),updated_by_id=1 WHERE wallet_user_id_id= _record.user_id_id;   
	    ELSE 
		INSERT INTO wallet_transaction(created_at,updated_at,reference_id,reference_type,transaction_type,transaction_amount,transaction_datetime, 
		created_by_id,transaction_by_id,wallet_transfer_from_user_id_id,wallet_transfer_to_user_id_id,updated_by_id,transaction_closing_amount,transaction_opening_amount)
		VALUES (now(),now(),in_recharge_tran_id ,50,'D', _record.commision_amount_final,now(),1,1,_record.user_id_id,
		1,_record.user_id_id,temp_wallet_balance-_record.recharge_amount, temp_wallet_balance);

		UPDATE wallet  SET updated_at=now(), wallet_amount=wallet_amount - _record.commision_amount_final, 
		wallet_update_datetime=now(),updated_by_id=1 WHERE wallet_user_id_id= _record.user_id_id;  
	    END IF;	
	ELSIF _record.commision_status = 0 THEN
	    IF  _record.user_id_id= in_tran_by THEN
		INSERT INTO wallet_transaction(created_at,updated_at,reference_id,reference_type,transaction_type,transaction_amount,transaction_datetime, 
		created_by_id,transaction_by_id,wallet_transfer_from_user_id_id,wallet_transfer_to_user_id_id,updated_by_id,transaction_closing_amount,transaction_opening_amount)
		VALUES (now(),now(),in_recharge_tran_id,50,'C',_record.recharge_amount,now(),1,1,_record.user_id_id,1,	_record.user_id_id,
		temp_wallet_balance+_record.recharge_amount, temp_wallet_balance);

		UPDATE wallet  SET updated_at=now(), wallet_amount=wallet_amount+_record.recharge_amount, wallet_update_datetime=now(),updated_by_id=1 
		WHERE wallet_user_id_id= _record.user_id_id;    	    
	    ELSE
	     UPDATE user_commision SET commision_status = 2 ,  user_commision_remarks = 'Dispute' WHERE recharge_tran_his_id_id = _record.recharge_tran_his_id_id 
	     AND user_id_id = _record.user_id_id;

	    END IF;	

	END IF;

END LOOP;    
RETURN 1;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;