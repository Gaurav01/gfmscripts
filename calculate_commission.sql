﻿
CREATE OR REPLACE FUNCTION calculate_commission()
  RETURNS integer AS
$BODY$
DECLARE
user_name_type hstore;
temp_p_id integer;
temp_str character varying;
temp_sa_com numeric;
temp_sd_com numeric;
temp_dis_com numeric;
temp_ret_com numeric;
_record record;
temp_com_calc numeric;
BEGIN


FOR _record IN (SELECT  recharge_api_id_id,recharge_gateway_id_id,telecom_operator_id_id,recharge_tran_his_id,recharge_tran_by_id,recharge_amount  FROM recharge_transaction_history  WHERE recharge_status=10 AND recharge_commision_status=0) LOOP 

	SELECT profit_commision_percentage,superdistributor_commision_percentage,distributor_commision_percentage,retailer_commision_percentage 
        INTO temp_sa_com,temp_sd_com,temp_dis_com,temp_ret_com
	FROM api_commision_profit_structure WHERE recharge_api_id_id=_record.recharge_api_id_id  AND recharge_gateway_id_id=_record.recharge_gateway_id_id
	AND telecom_operator_id_id=_record.telecom_operator_id_id  ;

	SELECT ('"'||usertype||'"=>"'||id||'"')::hstore,parent_id INTO  user_name_type,temp_p_id FROM webapp_master_user WHERE id=_record.recharge_tran_by_id;
	WHILE temp_p_id IS NOT NULL LOOP	
		SELECT '"'||usertype||'"=>"'||id||'"',parent_id INTO  temp_str,temp_p_id FROM webapp_master_user WHERE id=temp_p_id;
		user_name_type=user_name_type||temp_str::hstore;
	END LOOP;

	IF char_length(user_name_type->'SPA')>0 THEN
	    temp_com_calc=_record.recharge_amount*temp_sa_com/100;
	    INSERT INTO user_commision( recharge_amount, commision_percentage, commision_tax, commision_amount_final, commision_status, commision_datetime, 
             recharge_api_id_id, recharge_tran_his_id_id, telecom_operator_id_id, user_id_id)
            VALUES(_record.recharge_amount,temp_sa_com,0.00,temp_com_calc,0,now(),_record.recharge_api_id_id,_record.recharge_tran_his_id,_record.telecom_operator_id_id, 		(user_name_type->'SPA')::int);
	ELSE
	     temp_com_calc=_record.recharge_amount*temp_sa_com/100;
	    INSERT INTO user_commision( recharge_amount, commision_percentage, commision_tax, commision_amount_final, commision_status, commision_datetime, 
             recharge_api_id_id, recharge_tran_his_id_id, telecom_operator_id_id, user_id_id)
            VALUES(_record.recharge_amount,temp_sa_com,0.00,temp_com_calc,0,now(),_record.recharge_api_id_id,_record.recharge_tran_his_id,_record.telecom_operator_id_id, 		1);	
	END IF;

	IF char_length(user_name_type->'SDS')>0 THEN
	    temp_com_calc=_record.recharge_amount*temp_sd_com/100;
	    INSERT INTO user_commision( recharge_amount, commision_percentage, commision_tax, commision_amount_final, commision_status, commision_datetime, 
             recharge_api_id_id, recharge_tran_his_id_id, telecom_operator_id_id, user_id_id)
            VALUES(_record.recharge_amount,temp_sd_com,0.00,temp_com_calc,0,now(),_record.recharge_api_id_id,_record.recharge_tran_his_id,_record.telecom_operator_id_id, 		(user_name_type->'SDS')::int);
	ELSE
	     temp_com_calc=_record.recharge_amount*temp_sd_com/100;
	    INSERT INTO user_commision( recharge_amount, commision_percentage, commision_tax, commision_amount_final, commision_status, commision_datetime, 
             recharge_api_id_id, recharge_tran_his_id_id, telecom_operator_id_id, user_id_id)
            VALUES(_record.recharge_amount,temp_sd_com,0.00,temp_com_calc,0,now(),_record.recharge_api_id_id,_record.recharge_tran_his_id,_record.telecom_operator_id_id, 		1);	
	END IF;

	IF char_length(user_name_type->'DIS')>0  THEN
	    temp_com_calc=_record.recharge_amount*temp_dis_com/100;
	    INSERT INTO user_commision( recharge_amount, commision_percentage, commision_tax, commision_amount_final, commision_status, commision_datetime, 
             recharge_api_id_id, recharge_tran_his_id_id, telecom_operator_id_id, user_id_id)
            VALUES(_record.recharge_amount,temp_dis_com,0.00,temp_com_calc,0,now(),_record.recharge_api_id_id,_record.recharge_tran_his_id,_record.telecom_operator_id_id, (user_name_type->'DIS')::int);
	ELSE
	     temp_com_calc=_record.recharge_amount*temp_dis_com/100;
	    INSERT INTO user_commision( recharge_amount, commision_percentage, commision_tax, commision_amount_final, commision_status, commision_datetime, 
             recharge_api_id_id, recharge_tran_his_id_id, telecom_operator_id_id, user_id_id,user_commision_remarks)
            VALUES(_record.recharge_amount,temp_dis_com,0.00,temp_com_calc,0,now(),_record.recharge_api_id_id,_record.recharge_tran_his_id,_record.telecom_operator_id_id,1,'Calculated');	
	END IF;

	IF char_length(user_name_type->'RET')>0 THEN
	    temp_com_calc=_record.recharge_amount*temp_ret_com/100;
	    INSERT INTO user_commision( recharge_amount, commision_percentage, commision_tax, commision_amount_final, commision_status, commision_datetime, 
             recharge_api_id_id, recharge_tran_his_id_id, telecom_operator_id_id, user_id_id)
            VALUES(_record.recharge_amount,temp_ret_com,0.00,temp_com_calc,0,now(),_record.recharge_api_id_id,_record.recharge_tran_his_id,_record.telecom_operator_id_id,(user_name_type->'RET')::int);
	ELSE
	     temp_com_calc=_record.recharge_amount*temp_ret_com/100;
	    INSERT INTO user_commision( recharge_amount, commision_percentage, commision_tax, commision_amount_final, commision_status, commision_datetime, 
             recharge_api_id_id, recharge_tran_his_id_id, telecom_operator_id_id, user_id_id)
            VALUES(_record.recharge_amount,temp_ret_com,0.00,temp_com_calc,0,now(),_record.recharge_api_id_id,_record.recharge_tran_his_id,_record.telecom_operator_id_id,1);	
	END IF;

	INSERT INTO admin_commission_wallet(
            user_commision_id, recharge_tran_his_id, recharge_api_id, telecom_operator_id, 
            user_id, recharge_amount, commision_percentage, commision_tax, 
            commision_amount_final, commision_status, commision_datetime, 
            pay_datetime, user_commision_remarks)

	SELECT user_commision_id, recharge_amount, commision_percentage, commision_tax, 
	commision_amount_final, commision_status, commision_datetime, 
        pay_datetime, recharge_api_id_id, recharge_tran_his_id_id, telecom_operator_id_id, 
        user_id_id, user_commision_remarks
	FROM user_commision WHERE recharge_tran_his_id_id= _record.recharge_tran_his_id;
    

	
	UPDATE recharge_transaction_history  SET recharge_commision_status =1 WHERE recharge_tran_his_id=_record.recharge_tran_his_id AND recharge_status=10 AND recharge_commision_status=0;
END LOOP;    
RETURN 1;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;