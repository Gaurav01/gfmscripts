﻿
CREATE OR REPLACE FUNCTION disburse_commission_after_midnight()
  RETURNS integer AS
$BODY$
DECLARE
temp_tran_ref_id bigint;
_record  record;
temp_wallet_balance numeric;
BEGIN

SELECT to_char(current_timestamp,'yymmddhhmmss')::bigint INTO temp_tran_ref_id;

FOR _record IN (SELECT SUM(commision_amount_final) as comm_amount,user_id_id FROM user_commision WHERE commision_status=0 AND commision_datetime::date<=current_date GROUP BY user_id_id ORDER BY user_id_id) LOOP

	-- Take present wallet balance
	SELECT wallet_amount INTO temp_wallet_balance FROM wallet WHERE wallet_user_id_id=_record.user_id_id;

	INSERT INTO wallet_transaction(created_at,updated_at,reference_id,reference_type,transaction_type,transaction_amount,transaction_datetime, 
         created_by_id,transaction_by_id,wallet_transfer_from_user_id_id,wallet_transfer_to_user_id_id,updated_by_id,transaction_closing_amount,transaction_opening_amount)
	VALUES (now(),now(),temp_tran_ref_id,60,'C',_record.comm_amount,now(),1,_record.user_id_id,1,_record.user_id_id,1,temp_wallet_balance+_record.comm_amount, temp_wallet_balance);

	UPDATE wallet  SET updated_at=now(), wallet_amount=wallet_amount+_record.comm_amount, wallet_update_datetime=now(),updated_by_id=1 
	WHERE wallet_user_id_id= _record.user_id_id;    

	UPDATE user_commision SET commision_status=10,pay_datetime=now() WHERE commision_status=0 AND commision_datetime::date<=current_date;

	UPDATE admin_commission_wallet SET commision_status=10,pay_datetime=now() WHERE commision_status=0 AND commision_datetime::date<=current_date;

	--UPDATE recharge_transaction_history  SET recharge_commision_status=10 WHERE recharge_tran_his_id=_record.recharge_tran_his_id_id;

END LOOP;   

RETURN 1;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;